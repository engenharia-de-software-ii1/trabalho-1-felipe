<?php 
  function mult($num1, $num2){
    return $num1 * $num2;
  }

  function associativity($num1, $num2, $k) {
    $partial1 = mult($num1, $num2);
    $result1 = mult($partial1, $k);
 
    $partial2 = mult($num2, $k);
    $result2 = mult($num1, $partial2);

    return $result1 == $result2;
  }

  function commutativity($num1, $num2){
    $result1 = mult($num1, $num2);
    $result2 = mult($num2, $num1);

    return $result1 == $result2;
  }

  function neuterElement($num){
    $result = mult($num, 1);

    return $result == $num;
  }

  function inverseMultiplication($num){
    return $num != 0 ? mult($num, 1/$num) == 1 : true;
  }

  function verifyMult($num1, $num2, $k){
    $isAssociative = associativity($num1, $num2, $k);
    $isCommutative = commutativity($num1, $num2);
    $hasNeuterElement = neuterElement($num1);
    $hasInverse = inverseMultiplication($num1);

    if($isAssociative && $isCommutative && $hasNeuterElement && $hasInverse)
      echo "The mult function is correct.";

    if(! $isAssociative) echo "It doesn't respect the associativity axiom.<br>";
    if(! $isCommutative) echo "It doesn't respect the commutativity axiom.<br>";
    if(! $hasNeuterElement) echo "It doesn't respect the neuter element's axiom.<br>";
    if(! $hasInverse) echo "It doesn't respect the inverse property multiplication axiom.<br>";
  }

  $num1 = $_GET['num1'];
  $num2 = $_GET['num2'];
  $operation = $_GET['operation'];

  if(strcasecmp($operation, "verifyMult") == 0){
    verifyMult($num1, $num2, 3);
  } else{
    echo $num1, " * ", $num2, " = ", mult($num1, $num2);
  }
?>