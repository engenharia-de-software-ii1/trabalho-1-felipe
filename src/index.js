const btnExecute = document.getElementById("btnExecute");

btnExecute.onclick = () => {
  const filenameMap = {
    "verifySum": "sum",
    "sum": "sum",
    "verifyMult": "mult",
    "mult": "mult"
  };

  const output = document.getElementById("result");
  const number1 =  document.getElementById("number1").value;
  const number2 = document.getElementById("number2").value;

  if(!number1 || !number2){
    return alert('Please, fill the number fields!');
  }

  const operation = Array.from(document.getElementsByName('operation'))
  .filter(option => option.checked)[0].value;

  const params = `operation=${operation}&num1=${number1}&num2=${number2}`;

  function myFunction() {
    output.innerHTML = this.responseText;
  }

  const myRequest = new XMLHttpRequest();
  myRequest.onreadystatechange = myFunction;
  myRequest.open("GET", `${filenameMap[operation]}.php/?`.concat(params));
  myRequest.send();
};