<?php 
  function sum($num1, $num2) {
    return $num1 + $num2;
  }

  function associativity($num1, $num2, $k) {
    $partial1 = sum($num1, $num2);
    $result1 = sum($partial1, $k);

    $partial2 = sum($num2, $k);
    $result2 = sum($num1, $partial2);

    return $result1 == $result2;
  }

  function commutativity($num1, $num2){
    $result1 = sum($num1, $num2);
    $result2 = sum($num2, $num1);

    return $result1 == $result2;
  }

  function neuterElement($num1){
    $result = sum($num1, 0);

    return $result == $num1;
  }

  function symmetry($num1){
    $result = sum($num1, -$num1);

    return $result == 0;
  }

  function verifySum($num1, $num2, $k){
    $isAssociative = associativity($num1, $num2, $k);
    $isCommutative = commutativity($num1, $num2);
    $hasNeuterElement = neuterElement($num1);
    $isSymmetric = symmetry($num1);

    if($isAssociative && $isCommutative && $hasNeuterElement && $isSymmetric)
      echo "The sum function is correct.";

    if(! $isAssociative) echo "It doesn't respect the associativity axiom.<br>";
    if(! $isCommutative) echo "It doesn't respect the commutativity axiom.<br>";
    if(! $hasNeuterElement) echo "It doesn't respect the neuter element's axiom.<br>";
    if(! $isSymmetric) echo "It doesn't respect the symmetry's axiom.<br>";
  }
 
  $num1 = $_GET['num1'];
  $num2 = $_GET['num2'];
  $operation = $_GET['operation'];

  if($operation == 'verifySum'){
    verifySum($num1, $num2, 3);
  } else{
    echo $num1, " + ", $num2, " = ", sum($num1, $num2);
  }
?>